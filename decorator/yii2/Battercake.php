<?php

namespace app\pattern\decorator\yii2;

use app\pattern\decorator\Food;
use yii\base\Component;

/**
 * 只定义一个煎饼果子类
 * Class Battercake
 * @package app\pattern\decorator\impl
 */
class Battercake extends Component implements Food
{
    public $name = '煎饼果子';

    public function getDesc()
    {
        return $this->name;
    }
}
