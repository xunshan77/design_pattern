<?php


namespace app\pattern\decorator\yii2;


use yii\base\Behavior;

class EggBehavior extends Behavior
{
    /**
     * @param Battercake $cake
     * @return mixed
     */
    public function addEgg($cake)
    {
        $cake->name .= ' 加一个鸡蛋';
        return $cake;
    }
}
