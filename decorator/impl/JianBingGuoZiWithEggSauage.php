<?php

namespace app\pattern\decorator\impl;

/**
 * 煎饼果子加鸡蛋 加香肠
 * Class JianBingGuoZiWithEggSauage
 * @package app\pattern\decorator\impl
 */
class JianBingGuoZiWithEggSauage extends JianBingGuoZiWithEgg
{
    public function getDesc()
    {
        return parent::getDesc() . ' 加一根香肠';
    }
}
