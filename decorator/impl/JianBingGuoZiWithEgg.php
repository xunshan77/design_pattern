<?php

namespace app\pattern\decorator\impl;

/**
 * 加鸡蛋的煎饼果子
 * Class JianBingGuoZiWithEgg
 * @package app\pattern\decorator\impl
 */
class JianBingGuoZiWithEgg extends JianBingGuoZi
{
    public function getDesc()
    {
        return parent::getDesc() . ' 加一个鸡蛋';
    }
}
