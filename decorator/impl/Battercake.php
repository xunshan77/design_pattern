<?php

namespace app\pattern\decorator\impl;

use app\pattern\decorator\Food;

/**
 * 只定义一个煎饼果子类
 * Class Battercake
 * @package app\pattern\decorator\impl
 */
class Battercake implements Food
{
    private $decorators = [];
    public $name = '煎饼果子';

    public function addDecorator($decorator)
    {
        $this->decorators[] = $decorator;
    }

    private function before()
    {
        foreach ($this->decorators as $decorator) {
            $decorator->before();
        }
    }

    private function after()
    {
        foreach ($this->decorators as $decorator) {
            $decorator->after();
        }
    }

    //这里只定义煎饼
    public function getDesc()
    {
        $this->before();
        //do something else
        $this->after();
        return $this->name;
    }
}
