<?php

namespace app\pattern\decorator\impl;

use app\pattern\decorator\Decorator;

/**
 * 香肠装饰器
 * Class SauageDecorator
 * @package app\pattern\decorator\impl
 */
class SauageDecorator implements Decorator
{
    /**
     * @var Battercake
     */
    public $cake = null;

    public function __construct($cake)
    {
        $this->cake = $cake;
    }

    public function before()
    {
        //do nothing
    }

    public function after()
    {
        $this->cake->name .= ' 加一跟香肠';
    }
}
