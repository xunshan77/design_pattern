<?php

namespace app\pattern\decorator\impl;

use app\pattern\decorator\Food;

/**
 * 一份的煎饼果子
 * Class JianBingGuoZi
 * @package app\pattern\decorator\impl
 */
class JianBingGuoZi implements Food
{

    public function getDesc()
    {
        return '煎饼果子';
    }
}
