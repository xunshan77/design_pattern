<?php

namespace app\pattern\decorator\impl;

use app\pattern\decorator\Decorator;

/**
 * 鸡蛋装饰器
 * Class EggDecorator
 * @package app\pattern\decorator\impl
 */
class EggDecorator implements Decorator
{
    /**
     * @var Battercake
     */
    public $cake = null;
    public function __construct($cake)
    {
        $this->cake = $cake;
    }

    public function before()
    {
        //do nothing
    }

    public function after()
    {
        $this->cake->name .= ' 加一个鸡蛋';
    }
}
