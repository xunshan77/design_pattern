<?php

namespace app\pattern\decorator;

//装饰器接口
interface Decorator
{
    public function before();
    public function after();
}
