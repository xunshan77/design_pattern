<?php

namespace app\pattern\decorator;

/**
 * 定义一个实物的接口，1个方法，获取食物的名称
 * Interface Food
 * @package app\pattern\decorator
 */
interface Food
{
    //获取名称
    public function getDesc();
}
