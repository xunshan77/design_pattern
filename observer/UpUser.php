<?php

namespace app\pattern\observer;

/**
 * Up主
 * Class UpUser
 * @package app\pattern\observer
 */
class UpUser
{
    /**
     * 发布新视频了
     */
    public function publishVideo()
    {
        //通知各位
        Xiaoming::pleaseWatchVideo();
        XiaoLi::pleaseWatchVideo();
        ......
        // 还有好多其他的
    }
}

//up主发视频了
$uper = new UpUser();
$uper->publishVideo();
