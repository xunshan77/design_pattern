<?php

namespace app\pattern\observer;

/**
 * 订阅者、观察者
 * Interface Observer
 * @package app\pattern\observer
 */
interface Observer
{
    /**
     * 接收订阅消息
     * @return mixed
     */
    public function update();
}
