<?php

namespace app\pattern\observer;

/**
 * 发布者接口 被观察对象
 * Interface Subject
 * @package app\pattern\observer
 */
interface Subject
{
}
