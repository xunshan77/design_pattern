<?php

namespace app\pattern\observer\impl;

use app\pattern\observer\Observer;
use app\pattern\observer\Subject;
use yii\base\Component;

/**
 * up主的实现类
 * Class Uploader
 * @package app\pattern\observer\impl
 */
class Uploader extends Component implements Subject
{
    const EVENT_NAME = 'PUBLISH_VIDEO';

    public $name = '未填写信息的Up主';

    //发布视频后，通知所有订阅者
    public function publishVideo($name)
    {
        //$video = new Video();
        //$video->name = '战狼23';
        //$video->save(false);
        echo $this->name . '发布视频：' . $name . '，大家快来看' . PHP_EOL;
        $this->trigger(self::EVENT_NAME);
    }

}
