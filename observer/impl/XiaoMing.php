<?php

namespace app\pattern\observer\impl;

use app\pattern\observer\Observer;
use yii\base\Component;

class XiaoMing extends Component implements Observer
{
    public function update()
    {
        echo '小明看了1分钟就觉得没啥意思' . PHP_EOL;
    }
}

class XiaoLi extends Component implements Observer
{
    public function update()
    {
        echo '小李看完，激动的去了医院' . PHP_EOL;
    }
}

class XiaoHong extends Component implements Observer
{
    public function update()
    {
        echo '小红看完打开豆瓣评了2星' . PHP_EOL;
    }
}

class DaYe extends Component implements Observer
{
    public function update()
    {
        echo '大爷逼格高，都没看' . PHP_EOL;
    }
}
