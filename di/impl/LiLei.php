<?php


namespace app\pattern\di\impl;


use app\pattern\di\Person;

class LiLei extends Person
{
    public $name;
    public $phone;

    public function __construct($name, $config = [])
    {
        $this->name = $name;
        parent::__construct($config);
    }

    public function read()
    {
        $this->phone = new Iphone6s();
        echo $this->name . '拿出 ' . $this->phone->readBook();
    }

    public function play()
    {
        $this->phone = new Iphone6s();
        echo $this->name . '拿出 ' . $this->phone->playGame();
    }

    public function grab()
    {
        $this->phone = new Iphone6s();
        echo $this->name . '拿出 ' . $this->phone->grabRed();
    }
}
