<?php

namespace app\pattern\di\impl;

use app\pattern\di\Phone;
use yii\base\BaseObject;

class Iphone11ProMax extends BaseObject implements Phone
{
    private $name = 'iphone 11 pro max';

    public function readBook()
    {
        return $this->name . ' 开启 读书App';
    }

    public function playGame()
    {
        return $this->name . ' 开启 王者荣耀';
    }

    public function grabRed()
    {
        return $this->name . ' 开启 微信';
    }
}
