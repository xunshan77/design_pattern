<?php

namespace app\pattern\di\impl;

use app\pattern\di\Person;
use app\pattern\di\Phone;

/**
 * Class Hanmeimei
 * @package app\pattern\di\impl
 * @property $name;
 * @property Phone $phone
 */
class PersonWithPhone extends Person
{
    public $name;
    public $phone;

    public function __construct(Phone $phone, $name = '', $config = [])
    {
        $this->name = $name;
        $this->phone = $phone;
        parent::__construct($config);
    }

    public function read()
    {
        echo $this->name . '拿出 ' . $this->phone->readBook();
    }

    public function play()
    {
        echo $this->name . '拿出 ' . $this->phone->playGame();
    }

    public function grab()
    {
        echo $this->name . '拿出 ' . $this->phone->grabRed();
    }
}
