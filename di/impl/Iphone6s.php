<?php

namespace app\pattern\di\impl;

use app\pattern\di\Phone;
use yii\base\BaseObject;

class Iphone6s extends BaseObject implements Phone
{
    private $name = 'iphone 6s';

    private static $broken = false;

    public static function setBroken($isBroken)
    {
        self::$broken = $isBroken;
    }

    public static function isBroken()
    {
        return self::$broken;
    }

    public function readBook()
    {
        if (static::$broken) {
            return '坏掉的' . $this->name;
        }
        return $this->name . ' 开启 读书App';
    }

    public function playGame()
    {
        if (static::$broken) {
            return '坏掉的' . $this->name;
        }
        return $this->name . ' 开启 王者荣耀';
    }

    public function grabRed()
    {
        if (static::$broken) {
            return '坏掉的' . $this->name;
        }
        return $this->name . ' 开启 微信';
    }
}
