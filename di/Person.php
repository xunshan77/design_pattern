<?php

namespace app\pattern\di;


use yii\base\BaseObject;

/**
 * Class Person
 * @package app\pattern\di
 */
abstract class Person extends BaseObject
{
    //名字
    public $name;
    /**
     * 读书
     */
    public function read(){}

    /**
     * 玩游戏
     */
    public function play(){}

    /**
     * 抢红包
     */
    public function grab(){}
}
