<?php


namespace app\pattern\di;


interface Phone
{
    public function readBook();

    public function playGame();

    public function grabRed();
}
